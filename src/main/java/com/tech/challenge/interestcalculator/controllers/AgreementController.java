package com.tech.challenge.interestcalculator.controllers;

import com.tech.challenge.interestcalculator.dto.Agreement;
import com.tech.challenge.interestcalculator.dto.CalculationRequest;
import com.tech.challenge.interestcalculator.dto.InterestResult;
import com.tech.challenge.interestcalculator.services.AgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Controller
public class AgreementController implements WebMvcConfigurer {

    @Autowired
    private AgreementService agreementService;

    @PostMapping("/calculateInterest")
    public String
    calcInterestResultResponseEntity(CalculationRequest calculationRequest, Model model) {
        double amount = Double.parseDouble(calculationRequest.getAmount());
        double repoRate = Double.parseDouble(calculationRequest.getRepoRate());

        InterestResult interestResult = agreementService.calculateInterest(amount, repoRate,
                calculationRequest.getAgreementType(), calculationRequest.getStartDate(), calculationRequest.getEndDate());

        model.addAttribute("calculationResult", interestResult);

        return "result";
    }

    @GetMapping("/")
    public String loadPage(Model model) {
        List<Agreement> agreements = agreementService.findAllAgreements();

        model.addAttribute("agreements", agreements);
        model.addAttribute("calculationRequest", new CalculationRequest());

        return "index";
    }

}
