package com.tech.challenge.interestcalculator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import java.util.Date;


@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "CalculateInterestRate",
                procedureName = "CalculateInterestRate",
                parameters = {
                        @StoredProcedureParameter(
                                name = "amount",
                                type = Double.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "agreementType",
                                type = Integer.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "startDate",
                                type = Date.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "endDate",
                                type = Date.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "repoRate",
                                type = Double.class,
                                mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "status",
                                type = Integer.class,
                                mode = ParameterMode.OUT),
                        @StoredProcedureParameter(
                                name = "failedReason",
                                type = String.class,
                                mode = ParameterMode.OUT),
                        @StoredProcedureParameter(
                                name = "interestAmt",
                                type = Double.class,
                                mode = ParameterMode.OUT),
                })
})
@Entity
@Table(name = "TBL_AGREEMENT")
public class Agreement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "AGR_TYPE")
    private String type;

    @Column(name = "FORMULA     ")
    private String formula;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

}
