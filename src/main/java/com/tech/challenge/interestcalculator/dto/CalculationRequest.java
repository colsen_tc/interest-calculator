package com.tech.challenge.interestcalculator.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class CalculationRequest {

    private String amount;
    private Integer agreementType;
    private String repoRate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getAgreementType() {
        return agreementType;
    }

    public void setAgreementType(Integer agreementType) {
        this.agreementType = agreementType;
    }

    public String getRepoRate() {
        return repoRate;
    }

    public void setRepoRate(String repoRate) {
        this.repoRate = repoRate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
