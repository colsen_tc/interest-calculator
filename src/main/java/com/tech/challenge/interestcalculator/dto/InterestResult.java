package com.tech.challenge.interestcalculator.dto;

public class InterestResult {

    private Integer status;
    private String failedResponse;
    private Double interestAmt;

    public InterestResult() {
    }

    public InterestResult(Integer status, String failedResponse, Double interestAmt) {
        this.status = status;
        this.failedResponse = failedResponse;
        this.interestAmt = interestAmt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getFailedResponse() {
        return failedResponse;
    }

    public void setFailedResponse(String failedResponse) {
        this.failedResponse = failedResponse;
    }

    public Double getInterestAmt() {
        return interestAmt;
    }

    public void setInterestAmt(Double interestAmt) {
        this.interestAmt = interestAmt;
    }
}
