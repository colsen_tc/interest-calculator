package com.tech.challenge.interestcalculator.dto;

public class Agreement {

    private Integer agreementType;
    private String agreementDescription;

    public Agreement(Integer agreementType, String agreementDescription) {
        this.agreementType = agreementType;
        this.agreementDescription = agreementDescription;
    }

    public Integer getAgreementType() {
        return agreementType;
    }

    public void setAgreementType(Integer agreementType) {
        this.agreementType = agreementType;
    }

    public String getAgreementDescription() {
        return agreementDescription;
    }

    public void setAgreementDescription(String agreementDescription) {
        this.agreementDescription = agreementDescription;
    }
}
