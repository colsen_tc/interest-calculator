package com.tech.challenge.interestcalculator.repositories;

import com.tech.challenge.interestcalculator.model.Agreement;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface AgreementRepository extends CrudRepository<Agreement, Integer> {

    @Procedure(name = "CalculateInterestRate")
    Map<String, Object> calculateInterest(@Param("amount") Double amount, @Param("repoRate") Double repoRate,
            @Param("agreementType") Integer agreementType, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<Agreement> findAll();
}
