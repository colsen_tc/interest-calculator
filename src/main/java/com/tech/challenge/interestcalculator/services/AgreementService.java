package com.tech.challenge.interestcalculator.services;

import com.tech.challenge.interestcalculator.dto.Agreement;
import com.tech.challenge.interestcalculator.dto.InterestResult;
import com.tech.challenge.interestcalculator.repositories.AgreementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class AgreementService {

    @Autowired
    private AgreementRepository agreementRepository;

    public InterestResult calculateInterest(Double amount, Double repoRate, Integer agreementType, Date startDate, Date endDate) {
        Map<String, Object> interestResultMap = agreementRepository.calculateInterest(amount, repoRate, agreementType, startDate, endDate);

        Integer status = (Integer) interestResultMap.get("status");
        String failedReason = (String) interestResultMap.get("failedReason");
        Double interestAmt = (Double) interestResultMap.get("interestAmt");


        return new InterestResult(status, failedReason, interestAmt);
    }

    public List<Agreement> findAllAgreements() {
        List<com.tech.challenge.interestcalculator.model.Agreement> allAgreementDOs = agreementRepository.findAll();

        List<Agreement> agreements = new ArrayList<>();
        for (com.tech.challenge.interestcalculator.model.Agreement allAgreementDO : allAgreementDOs) {
            agreements.add(new Agreement(allAgreementDO.getId(), allAgreementDO.getType()));
        }

        return agreements;
    }

}
