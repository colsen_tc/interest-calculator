# DROP TABLE TBL_AGREEMENT;

CREATE TABLE TBL_AGREEMENT
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    AGR_TYPE VARCHAR(255),
    FORMULA  VARCHAR(255)
);

INSERT INTO TBL_AGREEMENT (AGR_TYPE, FORMULA)
VALUES ('Mortgage agreements', '(RR * 2.2) + (5/100)'),
       ('Credit facilities', '(RR * 2.2) + (10/100)'),
       ('Unsecured credit transactions', '(RR * 2.2) + (20/100)'),
       ('Developmental credit agreements For development of a small business', '(RR * 2.2) + (20/100)'),
       ('Developmental credit agreements For low income housing (unsecured)', '(RR * 2.2) + (20/100)'),
       ('Short term credit transactions', '(RR * 2.2) + (5/100)'),
       ('Other credit agreements', '(RR * 2.2) + (10/100)'),
       ('Incidental credit agreements', '(RR * 2.2) + (2/100)');
