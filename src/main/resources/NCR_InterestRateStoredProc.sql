DROP PROCEDURE IF EXISTS CalculateInterestRate;

DELIMITER $$
CREATE PROCEDURE CalculateInterestRate(IN amount DECIMAL(16, 2), IN agreementType INT, IN startDate DATE,
                                       IN endDate DATE, IN repoRate DECIMAL(16, 2),
                                       OUT status INT, OUT failedReason VARCHAR(500), OUT interestAmt DECIMAL(16, 2))
proc: BEGIN
    DECLARE formula VARCHAR(255);
    DECLARE noOfDays INT;
    DECLARE interestPercentage DECIMAL(16, 2);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
        BEGIN
            GET DIAGNOSTICS CONDITION 1
                @p1 = MESSAGE_TEXT;
            SELECT 0    AS status,
                   @p1  AS failedReason,
                   NULL AS interestAmt;
            ROLLBACK;
        END;

    DECLARE EXIT HANDLER FOR SQLWARNING
        BEGIN
            GET DIAGNOSTICS CONDITION 1
                @p2 = MESSAGE_TEXT;
            SELECT 0    AS status,
                   @p2  AS failedReason,
                   NULL AS interestAmt;
            ROLLBACK;
        END;

    IF amount IS NULL THEN
        SELECT 0                             AS status,
               'Amount needs to be provided' AS failedReason,
               NULL                          AS interestAmt;

        LEAVE proc;
    ELSEIF agreementType IS NULL THEN
        SELECT 0                                     AS status,
               'Agreement type needs to be provided' AS failedReason,
               NULL                                  AS interestAmt;
        LEAVE proc;
    ELSEIF startDate IS NULL THEN
        SELECT 0                                 AS status,
               'Start date needs to be provided' AS failedReason,
               NULL                              AS interestAmt;
        LEAVE proc;
    ELSEIF endDate IS NULL THEN
        SELECT 0                               AS status,
               'End Date needs to be provided' AS failedReason,
               NULL                            AS interestAmt;
        LEAVE proc;
    ELSEIF repoRate IS NULL THEN
        SELECT 0                                AS status,
               'Repo rate needs to be provided' AS failedReason,
               NULL                             AS interestAmt;
        LEAVE proc;
    ELSE
        SELECT FORMULA
        INTO formula
        FROM TBL_AGREEMENT
        WHERE id = agreementType;
    END IF;

    SELECT DATEDIFF(endDate, startDate) INTO noOfDays;

    SELECT REPLACE(formula, 'RR', repoRate) INTO formula;
    SELECT CONCAT('SELECT ', formula) INTO formula;
    SELECT CONCAT(formula, ' / 100 INTO interestPercentage;') INTO formula;

    PREPARE calcPercentage FROM @formula;
    EXECUTE calcPercentage;

    SELECT 1                                              AS status,
           (amount * interestPercentage) * noOfDays / 365 AS interestAmt,
           NULL                                           AS failedResponse;
END $$

DELIMITER ;

CALL CalculateInterestRate(500.12, 1, DATE('2020-01-01'), DATE('2020-02-27'), 9.75,
                           @status, @failedReason, @interestAmt);
